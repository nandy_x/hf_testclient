﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HFService.TestClient.ServiceContract;
using HFService.TestClient.Model;

namespace HFService.TestClient.Controllers
{
    public class HomeController : Controller
    {
        private readonly IService service;

        public HomeController(IService service)
        {
            this.service = service;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Invoke([FromBody]ServiceModel request)
        {
            var response = string.Empty;

            if (ModelState.IsValid)
                response = await service.Invoke(request);

            return Json(response);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
