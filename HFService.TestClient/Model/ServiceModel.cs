﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HFService.TestClient.Model
{
    public class ServiceModel
    {
        public string UrlHost { get; set; }

        public string Request { get; set; }
    }
}
