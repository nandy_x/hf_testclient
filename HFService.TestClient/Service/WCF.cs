﻿using HFService.TestClient.Model;
using HFService.TestClient.ServiceContract;
using System.Net;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace HFService.TestClient.Service
{
    public class WCF : IService
    {
        public async Task<string> Invoke(ServiceModel model)
        {
            string result = string.Empty;

            var serializer = new JavaScriptSerializer();

            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                result = await client.UploadStringTaskAsync(model.UrlHost, "POST", model.Request);
            }
            return result;

        }
    }
}
