﻿using HFService.TestClient.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HFService.TestClient.ServiceContract
{
    public interface IService
    {
        Task<string> Invoke(ServiceModel request);
    }
}
