﻿; (() => {

    const form = $('#service-form');
    form.validate();
    jQuery.validator.setDefaults({
        debug: true,
        success: "valid"
    });
    $("#clear").on('click', () => {
        App.clear();
    });
    $("#invoke").on('click', () => {
        if (form.valid()) {
            App.send();
        }
    });
    $("#copy-request").on('click', () => {
        App.copy(true);
    });
    $("#copy-response").on('click', () => {
        App.copy(false);
    });
    var App = (() => {
        let host = $('#host'),
            request = $('#request'),
            response = $('#response'),
            _this = this;
        return {
            clear: () => {
                host.val("");
                request.val("");
                response.val("");
            },
            send: () => {
                var model = {
                    UrlHost: host.val(),
                    Request: request.val()
                };

                return $.ajax({
                    url: '/Home/Invoke',
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: JSON.stringify(model),
                    success: function (data) {
                        response.val(data);
                    },
                    failure: function (response) {
                        response.val("Los datos proporcionados no son correctos o el servicio no se encuentra disponible");
                    },
                    error: function (response) {
                        response.val("Los datos proporcionados no son correctos o el servicio no se encuentra disponible");
                    }
                });
            },
            copy: (a) => {
                var copyTextarea = a == true ? document.querySelector('#request')
                    : document.querySelector('#response');
                copyTextarea.select();
                try {
                    var successful = document.execCommand('copy');
                } catch (err) {
                    console.log('Oops, unable to copy');
                }
            }
        }
    })();

})();